**Event Landing Page**

[https://gregfreeman.me/projects/event-landing-page/](https://gregfreeman.me/projects/event-landing-page/)

---

- Single Page Responsive Event Landing Page
- HTML, CSS, JS, jQuery
- Lazyload Images
- Delayed Modal with Cookie
- Slick Slider
- Event Schema Markup
